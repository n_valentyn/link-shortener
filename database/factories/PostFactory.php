<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Helper\Text;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(\App\Post::class, function (Faker $faker) {
    $user = \App\User::inRandomOrder()->limit(1)->get()->first();
    return [
        'id' => Str::uuid()->toString(),
        'user_id' => $user->id,
        'text' => Text::replaceLinks($faker->text . ' ' . $faker->url . ' ' . $faker->url, $user),
    ];
});
