<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Statistic;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Statistic::class, function (Faker $faker) {
    $userAgentInfo = new WhichBrowser\Parser($ua = $faker->userAgent);
    return [
        'id' => Str::uuid(),
        'link_id' => \App\Link::inRandomOrder()->limit(1)->get()->first()->id,
        'ip' => $faker->ipv4,
        'country_name' => $faker->country,
        'country_code' => $faker->countryCode,
        'city_name' => $faker->city,
        'user_agent' => $ua,
        'browser' => $userAgentInfo->browser->name,
        'engine' => $userAgentInfo->engine->name,
        'os' => $userAgentInfo->os->name,
        'device' => $userAgentInfo->device->type
    ];
});
