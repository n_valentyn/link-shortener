<?php

namespace App\Http\Controllers;

use App\Jobs\ProcessStatistic;
use Illuminate\Http\Request;

class RedirectController extends Controller
{
    public function __invoke($code, Request $request)
    {
        $link = \App\Link::getLink($code);

        if ($link === null) {
            abort(404);
            // return redirect('/');
        }

        ProcessStatistic::dispatch($link->id, $request->ip(), $request->userAgent(), now());

        return redirect()->away($link->source_link);
    }
}
