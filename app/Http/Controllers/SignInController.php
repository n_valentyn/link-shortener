<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class SignInController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index()
    {
        return view('sign-in');
    }

    public function handle(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required|email:rfc,dns',
            'password' => 'required|min:6|max:255'
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials, $request->boolean('remember'))) {
            return redirect()->intended('/');
        } else {
            throw ValidationException::withMessages([
                'email' => [trans('auth.failed')],
            ]);
        }
    }

    public function google(Request $request)
    {
        $client = new \Google_Client();
        $client->setAuthConfig(__DIR__ . '/../../../' . env('OAUTH_GOOGLE_CLIENT_SECRET_PATH'));
        $client->addScope(["https://www.googleapis.com/auth/userinfo.email", "https://www.googleapis.com/auth/userinfo.profile"]);
        $client->setAccessType('offline');

        if(!$request->has('code')){
            $auth_url = $client->createAuthUrl();
            header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
        } else {

            $client->authenticate($request->get('code'));
            // $access_token = $client->getAccessToken();
            //  dd($access_token);
            // $access_token = 'ya29.a0Ae4lvC2UEzgYw1mLRqLY2h0Ih5n3cYe5nBO7LdQ8bYlysdDR2PlGAWv_n2-uZVFdMDUPuRuT7y0QncJ8gMcKpwxvKV1Z6XLJwjeQ151hVI3SbT1n3bZNRuldOgX55XCKYROTqmBE3lO-rhGc-tl1BRgxhXVs04syvGDq';
            // $client->setAccessToken($access_token);

            $oauth2 = new \Google_Service_Oauth2($client);
            $userInfo = $oauth2->userinfo->get();

            if (!$userInfo->verifiedEmail) {
                return redirect()->route('sign-up')->with(['error' => 'Google account email not verified!']);
            }

            $user = User::firstWhere('email', $userInfo->email);

            if (!$user) {
                $user = new User();
                $user->name = $userInfo->name;
                $user->email = $userInfo->email;
                $user->password = Hash::make(Str::random(10));
                $user->email_verified_at = now();
                $user->save();
            }

            Auth::login($user, true);

            return redirect()->intended('/');
        }
    }

    public function logout() {
        Auth::logout();
        return redirect()->route('home');
    }
}
