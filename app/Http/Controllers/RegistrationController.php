<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class RegistrationController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index()
    {
        return view('sign-up');
    }

    public function handle(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|min:3|max:255|string',
            'email' => 'required|email:rfc,dns|unique:users,email',
            'password' => 'required|min:6|max:255|confirmed'
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->email_verified_at = '2020-03-14 20:03:12';
        $user->save();

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return redirect()->route('home');
        }
    }
}
