<?php

namespace App\Http\Controllers;

use App\Helper\Text;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class WallController extends Controller
{
    public function index()
    {
        return view('wall', ['posts' => Auth::user()->posts()->latest()->get()]);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'post' => 'required|min:3|max:1024|string'
        ]);

        Auth::user()->posts()->create([
            'id' => Str::uuid()->toString(),
            'text' => Text::replaceLinks($request->post, Auth::user()),
        ]);

        return redirect()->route('wall')->with('status', 'Post created!');
    }
}
