<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;

class Link extends Model
{
    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    public function statistics()
    {
        return $this->hasMany(\App\Statistic::class);
    }

    public static function getLink(string $shortCode): Link
    {
        $key = 'link:' . $shortCode;

        if (Cache::has($key)) {
            return Cache::get($key);
        }

        $link = \App\Link::where('short_code', $shortCode)->first();

        Cache::put($key, $link, now()->addHour(24));

        return $link;
    }
}
