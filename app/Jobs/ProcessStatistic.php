<?php

namespace App\Jobs;

use App\Statistic;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Str;

class ProcessStatistic implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    protected $linkId;
    protected $ip;
    protected $userAgent;
    protected $createdAt;
    protected $statistic;

    public function __construct($linkId, $ip, $userAgent, $createdAt)
    {
        $this->linkId = $linkId;
        $this->ip = $ip;
        $this->userAgent = $userAgent;
        $this->createdAt = $createdAt;
    }

    /**
     * Execute the job.
     *
     * @param \AdditionComponentsNVM\IpParser\AdapterInterface $IpParser
     * @param \AdditionComponentsNVM\UAParser\AdapterInterface $UAParser
     * @return void
     */
    public function handle(
        \AdditionComponentsNVM\IpParser\AdapterInterface $IpParser,
        \AdditionComponentsNVM\UAParser\AdapterInterface $UAParser
    )
    {
        if(!$IpParser->parse($this->ip)){
            $IpParser->parse(env('DEFAULT_IP_ADDR'));
        }
        $UAParser->parse($this->userAgent);

        $statistic = new Statistic;
        $statistic->id = Str::uuid()->toString();
        $statistic->link_id = $this->linkId;
        $statistic->ip = $this->ip;
        $statistic->user_agent = $this->userAgent;
        $statistic->country_name = $IpParser->getCountryName();
        $statistic->country_code = $IpParser->getCountryCode();
        $statistic->city_name = $IpParser->getCityName();
        $statistic->browser = $UAParser->getBrowser();
        $statistic->engine = $UAParser->getEngine();
        $statistic->os = $UAParser->getOs();
        $statistic->device = $UAParser->getDevice();
        $statistic->created_at = $this->createdAt;
        $statistic->save();
    }
}
