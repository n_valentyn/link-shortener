<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //IP Parser
        $this->app->singleton(\AdditionComponentsNVM\IpParser\AdapterInterface::class, function (){
            //$reader = new \GeoIp2\Database\Reader(resource_path() . '/GeoLite2/GeoLite2-City.mmdb');
            //return new \AdditionComponentsNVM\IpParser\MaxMindAdapter($reader);

            return new \AdditionComponentsNVM\IpParser\IpApiAdapter();
        });

        //User Agent Parser
        $this->app->singleton(\AdditionComponentsNVM\UAParser\AdapterInterface::class, function (){
            return new \AdditionComponentsNVM\UAParser\WhichBrowserAdapter();
            //return new \AdditionComponentsNVM\UAParser\UAParserAdapter();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
