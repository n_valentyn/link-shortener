<?php

namespace App\Helper;

use App\Link;
use App\User;

class Text
{
    public static function replaceLinks($text, User $user)
    {
        preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $text, $match);

        $newText = $text;

        foreach($match[0] as $sourceLink) {
            $link = new Link();
            $link->short_code = uniqid();
            $link->source_link = $sourceLink;

            $user->links()->save($link);

            $newText = str_replace(
                $sourceLink,
                '<a href="' . env('APP_URL') . '/r/' . $link->short_code . '"  target="_blank">' . $sourceLink . '</a>',
                $newText
            );
        }

        return $newText;

    }
}
