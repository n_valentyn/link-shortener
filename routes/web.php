<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return view('home');
})->name('home');

Route::get('/test', function () {
    $statistic = \App\Statistic::inRandomOrder()->limit(1)->get()->first();

    echo '<h4>Statistic</h4>';
    var_dump($statistic->__toString());

    echo '<h4>Statistic->Link</h4>';
    var_dump($statistic->link->__toString());

    echo '<h4>Statistic->Link->User</h4>';
    var_dump($statistic->link->user->__toString());

    echo '<h4>Link->Statistics</h4>';
    var_dump($statistic->link->statistics->__toString());

    echo '<h4>User->Links</h4>';
    var_dump($statistic->link->user->links->__toString());
});

Route::get('/r/{code}', '\\' . \App\Http\Controllers\RedirectController::class)->name('redirect');

Route::get('/sign-up', '\\' . \App\Http\Controllers\RegistrationController::class . '@index')->name('sign-up');
Route::post('/sign-up', '\\' . \App\Http\Controllers\RegistrationController::class . '@handle')->name('sign-up-handle');

Route::get('/sign-in', '\\' . \App\Http\Controllers\SignInController::class . '@index')->name('sign-in');
Route::post('/sign-in', '\\' . \App\Http\Controllers\SignInController::class . '@handle')->name('sign-in-handle');
Route::get('/logout', '\\' . \App\Http\Controllers\SignInController::class . '@logout')
    ->name('logout')
    ->middleware('auth');

Route::get('/sign-in/google', '\\' . \App\Http\Controllers\SignInController::class . '@google')->name('sign-in.google');


Route::middleware('auth')->group(function (){
    Route::get('/wall', '\\' . \App\Http\Controllers\WallController::class . '@index')->name('wall');
    Route::post('/store-post', '\\' . \App\Http\Controllers\WallController::class . '@store')->name('store.post');
});





Route::get('/index', function(){
    echo '<pre>';
    echo '<b>With index user_id
explain select * from posts where user_id = 1 </b><br>
';

    $post = DB::select('explain select * from posts where user_id = 1');
    print_r($post);

    echo '<b>With index user_id and created_at
explain select * from posts where user_id = 1 and created_at = 2020-04-05 </b><br>
';

    $post = DB::select('explain select * from posts where user_id = 1 and created_at = \'2020-04-05 19:58:56\'');
    print_r($post);


    DB::statement('ALTER TABLE posts DROP FOREIGN KEY posts_user_id_foreign');
    DB::statement('ALTER TABLE `posts` DROP INDEX `posts_user_id_created_at_index`');
    DB::statement('ALTER TABLE `posts` DROP INDEX `posts_user_id_index`');


    echo '<b>Without index user_id
explain select * from posts where user_id = 1 </b><br>
';

    $post = DB::select('explain select * from posts where user_id = 1');
    print_r($post);

    echo '<b>Without index user_id and created_at
explain select * from posts where user_id = 1 and created_at = 2020-04-05 </b><br>
';

    $post = DB::select('explain select * from posts where user_id = 1 and created_at = \'2020-04-05 19:58:56\'');
    print_r($post);

    DB::statement('ALTER TABLE `posts` ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (user_id) REFERENCES users(id)');

    DB::statement('CREATE INDEX `posts_user_id_index` ON posts(user_id)');
    DB::statement('CREATE INDEX `posts_user_id_created_at_index` ON posts(user_id, created_at)');


})->name('index');
