@extends('layouts.app')

@section('title', 'Home - ' . env('APP_NAME'))

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-10 col-md-6 mt-3">

                <!--CREATE POST -->
                <div class="card">
                    <label for="post" class="mb-0"><h5 class="card-header">New post</h5></label>
                    <div class="card-body">
                        <form class="form" action="{{ route('store.post') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <textarea class="form-control @error('post') is-invalid @enderror" id="post" name="post" rows="3" required></textarea>
                                @error('post')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <button class="btn btn-primary btn-block" type="submit">Publish</button>
                        </form>
                    </div>
                </div>
                <!--END CREATE POST -->

                <!-- ALERT -->
                @if (session('status'))
                    <div class="alert alert-success mt-3">
                        {{ session('status') }}
                    </div>
                @endif
               <!-- END ALERT -->

                <!-- POSTS -->
                @foreach($posts as $post)
                <div class="card text-left mt-3">
                    <div class="card-body">
                        <p class="card-text" style="white-space: pre-wrap">{!! $post->text !!}</p>
                    </div>
                    <div class="card-footer text-muted py-1">
                        {{ ($days = (new DateTime())->diff(new DateTime($post->created_at))->format('%a')) > 0 ? $days . ' days ago' : 'Today' }}
                    </div>
                </div>
                @endforeach
                <!-- END POSTS -->
            </div>
        </div>
    </div>
@endsection
