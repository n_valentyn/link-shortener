<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>@yield('title')</title>
</head>
<body>
<nav class="navbar navbar-expand-sm navbar-light bg-light border">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ route('home') }}">
                <img src="/logo.png" class="d-inline-block align-top" alt=""  height="70"><br>
{{--                <span class="">Text</span>--}}
            </a>
        </div>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav mr-auto">
                <li class="nav-item {{ Route::currentRouteName() === 'wall' ? 'active' : ''}}">
                    <a class="nav-link text-uppercase font-weight-bold" href="{{ route('wall') }}"> Wall <span class="sr-only">(current)</span></a>
                </li>
                <!--                        <li class="nav-item">
                                           <span class="nav-link">|</span>
                                        </li>-->

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                @auth
                    <li class="nav-item disabled">
                        <a class="nav-link text-uppercase disabled font-weight-bold"> {{ Auth::user()->name }} </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link text-uppercase font-weight-bold" href="{{ route('logout') }}"> Log out </a>
                    </li>
                @endauth

                @guest
                <!-- Authentication Links -->
                <li class="nav-item ">
                    <a class="nav-link text-uppercase font-weight-bold {{ Route::currentRouteName() === 'sign-in' ? 'active' : ''}}" href="{{ route('sign-in') }}"> Sign in </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link text-uppercase font-weight-bold {{ Route::currentRouteName() === 'sign-up' ? 'active' : ''}}" href="{{ route('sign-up') }}"> Sign up </a>
                </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
@yield('content')

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
