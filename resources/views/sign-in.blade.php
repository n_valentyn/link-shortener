@extends('layouts.app')

@section('title', 'Sign in - ' . env('APP_NAME'))

@section('content')
    <!-- ALERT -->
    @if (session('error'))
        <div class="row justify-content-center">
            <div class="alert alert-danger mt-3" style="margin-bottom: -30px">
                {{ session('error') }}
            </div>
        </div>
    @endif
    <!-- END ALERT -->
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-10 col-md-6 mt-5">
                <div class="card">
                    <h5 class="card-header">Registration</h5>
                    <div class="card-body">
                        <form class="form" action="{{ route('sign-in-handle') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input value="{{ old('email') }}" name="email" type="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="example@gmail.com" required>
                                @error('email')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input name="password" type="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="" required>
                                @error('password')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group form-check">
                                <input type="checkbox" class="form-check-input" id="remember" name="remember" value="true" checked>
                                <label class="form-check-label" for="remember">Remember me</label>
                            </div>
                            <button class="btn btn-primary btn-block" type="submit">Sign in</button>
                        </form>

                        <div class="col-12">
                            <div class="row justify-content-center  mt-2 mb-1" style="font-size:1.25rem">
                                <div class="col"><hr></div>
                                <div class="col-auto">OR</div>
                                <div class="col"><hr></div>
                            </div>
                            <div class="row justify-content-center">
                                <a href="{{ route('sign-in.google') }}"> <img src="file/btn_google_signin_dark_normal_web.png"
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

